# Purpose

The purpose of this module is to tackle the customs problem that may arise from
decoupling the WCMS with a custom frontend.

Specify which path should NOT use the front-end decouple theme. These are
non-admin pages. Admin pages default to using the admin theme.
